const express = require('express');
const http = require('http')

const router = require('./v1/routes/route')
const app = express();

app.use(express.json());
app.use('/',router);

const server = http.createServer(app)
const io = require('socket.io')(server)
const socket = require("./sockets/socket");
require('./common/connection');


server.listen(5000,() => {
    socket(io);
    console.log('server started at port 5000')
})