const mongoose = require('mongoose');

const chatSchema = new mongoose.Schema({

    roomId: {
        type: String
    },

    message: {
        type: String
    }
},

{timestamps: true}

) 

const chatModel = mongoose.model('chatModel', chatSchema);
module.exports= chatModel