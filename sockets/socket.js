const chatModel = require("../models/chatModel")
const roomChatModel = require("../models/roomChatModel")

module.exports = io => {

    io.on('connection', (socket) => {
        console.log("User Connected")

    socket.on('join_room',async(data) => {
       const senderId = data.senderId
       const recieverId = data.recieverId


       const findUser = await roomChatModel.findOne({
         $and : [
            {senderId : senderId},
            {recieverId : recieverId}
         ]
        })

        if(!findUser) {
            const joinRoom = await roomChatModel.create(data);
            console.log(joinRoom)

        const roomId = joinRoom._id;
        socket.join(roomId);
    }
    else{
        console.log("Room is already joined")
    }
    })

    socket.on('leave_room', async(data) => {
        const roomId = data.roomId;

        if(roomId){
            const findUser = await roomChatModel.findOne({_id : roomId})
            console.log(findUser._id);
            socket.leave(roomId);
        }

        else{
            console.log("User does not exist")
        }
       
    })

    socket.on('send_message_user', async(data) => {
        try{
            roomId = data.roomId
            const findRoom = await roomChatModel.findOne({_id : roomId})
            const msg = await chatModel.create({
                roomId : data.roomId,
                message: data.message
            })

            
            console.log(msg);

        }catch{
            
        }
    })

    socket.on('disconnect', function(){
        console.log('User disconnect')
     })
})

}