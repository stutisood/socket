const chatModel = require('../../models/chatModel')
const detailModel = require('../../models/userDetails')
const mongoose=require('mongoose')
require('../../common/connection')
module.exports.postDetails = async(req, res) => {
   console.log("hello")
   console.log(req.body.username)
    const data = await detailModel.create({
        username: req.body.username
    });
    console.log(data);

    return res.json({
        data
    })  
  
}

module.exports.getAllChats = async(req, res) => {
    const chats = await chatModel.find();
    res.json({chats});
}


module.exports.getChat = async(req, res) => {
    id = req.params.id
   
    //console.log(id);

    const chats = await chatModel.findOne({roomId : id}).sort({"createdAt" : -1})
    res.json({chats})
}


